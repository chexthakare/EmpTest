<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome_model extends CI_Model {

    function __construct() {
        $this->countryTbl = 'countries';
        $this->stateTbl = 'states';
        $this->cityTbl = '    cities';
    }

	public function get(){
      return $this->db->get("employee")->result();
    }

    public function add($data){
        $this->db->insert('employee',$data);
        return 1;
    }
    public function display()
    {
        return $this->db->get("employee")->result_array();
    }
    public function edit($employeeid)
    {
        $query = $this->db->query("SELECT * FROM employee WHERE employeeid = $employeeid");
        return $query->row();
    }
     public function delete($employeeid)
    {
        $this->db->delete('employee', array('employeeid' => $employeeid));
        return;
    }


    public function update()
    {
        $employeeid = $this->input->post('employeeid');

        $data = array(
            'employeeid' => $this->input->post('employeeid'),
            'fname' => $this->input->post('fname'),
            'lname' => $this->input->post('lname'),
            'gender' => $this->input->post('gender'),
            'email' => $this->input->post('email'),
            'empdob' => $this->input->post('empdob'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            
            );
        return $this->db->update('employee', $data, "employeeid = $employeeid");
    }

    /*
     * Get country rows from the countries table
     */
    function getCountries($params = array()){
        $this->db->select('c.id, c.name');
        $this->db->from($this->countryTbl.' as c');
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                if(strpos($key,'.') !== false){
                    $this->db->where($key,$value);
                }else{
                    $this->db->where('c.'.$key,$value);
                }
            }
        }
        $this->db->where('c.status','1');
        
        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

        //return fetched data
        return $result;
    }
    
    /*
     * Get state rows from the countries table
     */
    function getStates($params = array()){
        $this->db->select('s.id, s.name');
        $this->db->from($this->stateTbl.' as s');
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                if(strpos($key,'.') !== false){
                    $this->db->where($key,$value);
                }else{
                    $this->db->where('s.'.$key,$value);
                }
            }
        }
        
        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

        //return fetched data
        return $result;
    }
    
    /*
     * Get city rows from the countries table
     */
    function getCities($params = array()){
        $this->db->select('c.id, c.name');
        $this->db->from($this->cityTbl.' as c');
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                if(strpos($key,'.') !== false){
                    $this->db->where($key,$value);
                }else{
                    $this->db->where('c.'.$key,$value);
                }
            }
        }
        
        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

        //return fetched data
        return $result;
    }


    public function getCountryName($countryid){
        $name = $this->db->query("SELECT name FROM countries WHERE id = $countryid");
        return $name->row();
    }

    public function getStateName($stateid){
        $name = $this->db->query("SELECT name FROM states WHERE id = $stateid");
        return $name->row();
    }

    public function getCityName($cityid){
        $name = $this->db->query("SELECT name FROM cities WHERE id = $cityid");
        return $name->row();
    }




}
