<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('welcome_model');
        $this->load->helper('url');
    }

	public function index()
	{
		$data['employee'] = $this->welcome_model->display();
		$data['countries'] = $this->welcome_model->getCountries();
		$this->load->view('welcome_message',$data);
	}

	public function validate() {

        $json = array();
        $this->form_validation->set_rules('fname', 'First Name', 'required');
        $this->form_validation->set_rules('lname', 'Last Name', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');

        $this->form_validation->set_message('required', 'You missed the input {field}!');

        if (!$this->form_validation->run()) {
            $json = array(
                'fname' => form_error('fname', '<p class="mt-3 text-danger">', '</p>'),
                'lname' => form_error('lname', '<p class="mt-3 text-danger">', '</p>'),
                'gender' => form_error('gender', '<p class="mt-3 text-danger">', '</p>'),
                'username' => form_error('username', '<p class="mt-3 text-danger">', '</p>'),
                'email' => form_error('email', '<p class="mt-3 text-danger">', '</p>'),
                'password' => form_error('password', '<p class="mt-3 text-danger">', '</p>'),
                'confirm_password' => form_error('confirm_password', '<p class="mt-3 text-danger">', '</p>'),
                'code' => form_error('code', '<p class="mt-3 text-danger">', '</p>')
            );
        }
        else{

            $countryid = $this->input->post('country');
            $stateid = $this->input->post('state');
            $cityid = $this->input->post('city');

            $countryName = $this->welcome_model->getCountryName($countryid);
            $countryName = $countryName->name;
            $stateName = $this->welcome_model->getStateName($stateid);
            $stateName = $stateName->name;
            $cityName = $this->welcome_model->getCityName($cityid);
            $cityName = $cityName->name;

        	$data = array(
        		'fname' => $this->input->post('fname'),
        		'lname' => $this->input->post('lname'),
        		'gender' => $this->input->post('gender'),
        		'email' => $this->input->post('email'),
        		'empdob' => $this->input->post('empdob'),
                'country' => $countryName,
                'state' => $stateName,
                'city' => $cityName,
        		'username' => $this->input->post('username'),
        		'password' => $this->input->post('password'),
        		
        		);
        	$this->welcome_model->add($data);
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }

	public function display()
	{
		$data['employee'] = $this->welcome_model->display();
		$data['countries'] = $this->welcome_model->getCountries();
		$this->load->view('welcome_message',$data);
	}
	public function edit($employeeid)
	{
		$data['editemp'] = $this->welcome_model->edit($employeeid);
		$data['countries'] = $this->welcome_model->getCountries();
		$this->load->view('edit',$data);
		
	}

    public function delete($employeeid)
    {
        $this->welcome_model->delete($employeeid);
        $data['employee'] = $this->welcome_model->display();
        $data['countries'] = $this->welcome_model->getCountries();
        $this->load->view('welcome_message',$data);
        
    }

	public function update(){
		$this->welcome_model->update();
		$data['employee'] = $this->welcome_model->display();
		$data['countries'] = $this->welcome_model->getCountries();
		$this->load->view('welcome_message',$data);
    }

    public function getCountries(){
        $data['countries'] = $this->welcome_model->getCountries();
        $this->load->view('welcome_message', $data);
    }
    
    public function getStates(){
        $states = array();
        $country_id = $this->input->post('country_id');
        if($country_id){
            $con['conditions'] = array('country_id'=>$country_id);
            $states = $this->welcome_model->getStates($con);
        }
        echo json_encode($states);
    }
    
    public function getCities(){
        $cities = array();
        $state_id = $this->input->post('state_id');
        if($state_id){
            $con['conditions'] = array('state_id'=>$state_id);
            $cities = $this->welcome_model->getCities($con);
        }
        echo json_encode($cities);
    }

    public function emplist()
	{
		$data['employee'] = $this->welcome_model->display();
		$this->load->view('viewemp',$data);
	}

    /*public function getCountryName($countryid)
    {
        $countryName = $this->welcome_model->getCountryName($countryid);
        $countryName = $countryName->name;
        echo $countryName;
        exit();
    }*/
    


}
