<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>CRUD Operation</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.12.0.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  </head>
  <body>
    </br></br></br>
    <div class="container">
      <div class="row">
        <div class="col-sm-6 ml-auto mr-auto m-auto">
          <div class="card mt-5">
            <h5 class="card-header"></h5>
            <div class="card-body">
              <?php echo form_open('welcome/register', array('id' => 'form', 'role' => 'form'));?>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <?php echo form_input('fname', '', array('class' => 'form-control', 'placeholder' => 'Enter First Name', 'id' => 'input-fname'));?>
                    <div id="error"></div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?php echo form_input('lname', '', array('class' => 'form-control', 'placeholder' => 'Enter Last Name', 'id' => 'input-lname'));?>
                    <div id="error"></div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="radio-inline">
                    <input type="radio" name="gender" value="male" id="male">Male
                    </label>
                    <label class="radio-inline">
                    <input type="radio" name="gender" value="female" id="female">Female
                    </label>
                    <div id="error"></div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <?php echo form_input('username', '', array('class' => 'form-control', 'placeholder' => 'Enter Username', 'id' => 'input-username'));?>
                    <div id="error"></div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <!-- Country dropdown -->
                    <select id="country" name="country" class="form-control">
                      <option value="">Select Country</option>
                      <?php
                        if(!empty($countries)){
                            foreach($countries as $row){ 
                                echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
                            }
                        }else{
                            echo '<option value="">Country not available</option>';
                        }
                        ?>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <!-- State dropdown -->
                    <select id="state" name="state" class="form-control">
                      <option value="">Select country first</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <!-- City dropdown -->
                    <select id="city" name="city" class="form-control">
                      <option value="">Select state first</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <?php echo form_input('email', '', array('class' => 'form-control', 'placeholder' => 'Enter Email', 'id' => 'input-email'));?>
                    <div id="error"></div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <input type="text" name="empdob" id="datepicker" class="form-control" placeholder="DD-MM-YY">
                    <div id="error"></div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <?php echo form_password('password', '', array('class' => 'form-control', 'placeholder' => 'Enter Password', 'id' => 'input-password'));?>
                    <div id="error"></div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?php echo form_password('confirm_password', '', array('class' => 'form-control', 'placeholder' => 'Enter Confirm Password', 'id' => 'input-confirm_password'));?>
                    <div id="error"></div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <button type="button" class="btn btn-block btn-dark" id="form-submit-button">Add</button>
                  </div>
                </div>
              </div>
              <?php echo form_close();?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="container">
      <button id="button_2" value="val_2" name="but2">Display Record</button> 
      <button onclick="location.href='<?php echo base_url();?>welcome/emplist'">View Record</button> 
      </div>  
    </div>
    
    </br></br>
    <!-- result end -->
    <div id="emplist" class="container">
    </div>
    </br></br>
    <script type="text/javascript">
      $( document ).ready(function() {
          $('#error').html(" ");
      
          $('#form-submit-button').on('click', function (e) {
              e.preventDefault();
      
              $.ajax({
                  type: "POST",
                  url: "<?php echo site_url('welcome/validate');?>", 
                  data: $("#form").serialize(),
                  dataType: "json",  
                  success: function(data){
                      $.each(data, function(key, value) {
                          $('#input-' + key).addClass('is-invalid');
                          $('#input-' + key).parents('.form-group').find('#error').html(value);
                      });
                       $.ajax({
               type: 'get',
               url: "<?php echo base_url();?>index.php/Welcome/emplist/",
               dataType: 'html',
               success: function (html) {
                 $('#emplist').html(html);
                 $("#form")[0].reset();
               }
             });
                    
                  }
              });
          });
      
      });
      
      
      //test php 
      
      $("#button_2").click(function(e) {
          // prevent the default action when a nav button link is clicked
          e.preventDefault();
          // ajax query to retrieve the HTML view without refreshing the page.
          $.ajax({
            type: 'get',
            url: "<?php echo base_url();?>index.php/Welcome/emplist/",
            dataType: 'html',
            success: function (html) {
              $('#emplist').html(html);
            }
          });
        });
    </script>
    <script>
      // set the calender format
      $( function() {
      $('input[id$=datepicker]').datepicker({
      dateFormat: 'dd-mm-yy'
      });
      } );
      
      /* Populate data to state dropdown */
      $('#country').on('change',function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('index.php/welcome/getStates'); ?>',
                data:'country_id='+countryID,
                success:function(data){
                    $('#state').html('<option value="">Select State</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.name);           
                            $('#state').append(option);
                        });
                    }else{
                        $('#state').html('<option value="">State not available</option>');
                    }
                }
            }); 
        }else{
            $('#state').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select state first</option>'); 
        }
      });
      
      /* Populate data to city dropdown */
      $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('index.php/welcome/getCities'); ?>',
                data:'state_id='+stateID,
                success:function(data){
                    $('#city').html('<option value="">Select City</option>'); 
                    var dataObj = jQuery.parseJSON(data);
                    if(dataObj){
                        $(dataObj).each(function(){
                            var option = $('<option />');
                            option.attr('value', this.id).text(this.name);           
                            $('#city').append(option);
                        });
                    }else{
                        $('#city').html('<option value="">City not available</option>');
                    }
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
      });
    </script>
  </body>
</html>