<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>CRUD Operation</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.12.0.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  </head>
  <body>
  </br></br></br>
    <div class="container" id="empdata">
      <div class="row">
        <div class="col-sm-6 ml-auto mr-auto m-auto" id="hodm_table">
          <table class="table table-striped table-hover table-responsive">
            <tr>
            <thead>
              <td>First Name</td>
              <td>Last Name</td>
              <td>Email Name</td>
              <td>Birth Date</td>
              <td>Username</td>
              <td>Action</td>
            </thead>
            </tr>
            <tr>
              <?php foreach ($employee as $value) { ?>
            <tbody>
              <td><?php echo $value['fname'];?></td>
              <td><?php echo $value['lname'];?></td>
              <td><?php echo $value['email'];?></td>
              <td><?php echo $value['empdob'];?></td>
              <td><?php echo $value['username'];?></td>
              <td><a href="<?php echo site_url('Welcome/edit/'.$value['employeeid']);?>">Edit</a></td>
            </tbody>
            <?php  } ?>
            </tr>
          </table>
        </div>
      </div>
      <a href="javascript:window.history.go(-1);">Back</a>
    </div>

  </body>
</html>