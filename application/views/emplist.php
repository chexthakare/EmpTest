    <div class="container" id="empdata">
      <div class="row">
        <div class="col-sm-12 ml-auto mr-auto m-auto" id="hodm_table">
          <table class="table table-striped table-hover table-responsive">
            <tr>
            <thead>
              <td>First Name</td>
              <td>Last Name</td>
              <td>Email Name</td>
              <td>Birth Date</td>
              <td>Username</td>
              <td>Country</td>
              <td>State</td>
              <td>Ciry</td>
              <td>Action</td>
              <td>Delete</td>
            </thead>
            </tr>
            <tr>
              <?php foreach ($employee as $value) { ?>
            <tbody>
              <td><?php echo $value['fname'];?></td>
              <td><?php echo $value['lname'];?></td>
              <td><?php echo $value['email'];?></td>
              <td><?php echo $value['empdob'];?></td>
              <td><?php echo $value['username'];?></td>
              <td><?php echo $value['country'];?></td>
              <td><?php echo $value['state'];?></td>
              <td><?php echo $value['city'];?></td>
              <td><a href="<?php echo site_url('edit/'.$value['employeeid']);?>">Edit</a></td>
              <td><?=anchor("delete/".$value['employeeid'],"Delete",array('onclick' => "return confirm('Do you want delete this record')"))?></td>
            </tbody>
            <?php  } ?>
            </tr>
          </table>
        </div>
      </div>
    </div>

   